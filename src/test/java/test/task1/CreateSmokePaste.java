package test.task1;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.pastebin.MainPage;
import page.pastebin.ResultPage;
import test.CommonBeforeTest;

public class CreateSmokePaste extends CommonBeforeTest {
    // test data
    private String code = "Hello from WebDriver";
    private String expirationPeriod = "10 Minutes";
    private String title = "helloweb";

    @Test
    public void testCreateSmokePaste() {
        ResultPage resultPage = new MainPage(driver)
                .openPastebinPage()
                .closePopUp()
                .fillTextArea(code)
                .clickExpirationArrow()
                .selectExpirationPeriod(expirationPeriod)
                .fillTitle(title)
                .clickSubmitButton();

        Assert.assertTrue(resultPage.isPasteCreatedSuccessfully(), "Failed to create a new paste.");
    }
}
