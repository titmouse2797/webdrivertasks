package test.task3;

import driver.WebDriverManager;
import model.VirtualMachine;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import page.google.*;
import service.GoogleCloudService;
import util.BrowserUtils;
import util.StringUtils;
import util.TestDataReader;
import util.TestListener;

@Listeners({TestListener.class})
public class CalculateGoogleCloudMonthlyCostTest {
    public static final String TESTDATA_NUMBER_OF_INSTANCES = "number.of.instances";
    public static final String TESTDATA_INSTANCE_FOR_WHAT = "instances.for.what";
    public static final String TESTDATA_SOFTWARE = "software";
    public static final String TESTDATA_PROVISIONING_MODEL = "provisioning.model";
    public static final String TESTDATA_MACHINE_FAMILY = "machine.family";
    public static final String TESTDATA_SERIES = "series";
    public static final String TESTDATA_MACHINE_TYPE = "machine.type";
    public static final String TESTDATA_GPU_TYPE = "gpu.type";
    public static final String TESTDATA_NUMBER_OF_GPU = "number.of.gpu";
    public static final String TESTDATA_LOCAL_SSD = "local.ssd";
    public static final String TESTDATA_DATACENTER_LOCATION = "datacenter.location";
    public static final String TESTDATA_COMMITTED_USAGE = "committed.usage";
    public static final String TESTDATA_TOTAL_PRICE = "total.price";
    public static final String SEARCH_TEXT = "search.text";
    public static final String CALCULATOR_LINK = "calculator.link.name";
    public static final String TOTAL_PRICE = "total.price";

    private String totalCost = String.format("Total Estimated Cost: %s per 1 month",
            TestDataReader.getTestData(TOTAL_PRICE));
    private String totalMonthlyCost = String.format("Total Estimated Monthly Cost\n%s",
            TestDataReader.getTestData(TOTAL_PRICE));
    private String email;
    private VirtualMachine instance;

    @BeforeClass(alwaysRun = true)
    public void before() {
        email = StringUtils.generateEmail(5);
        instance = new VirtualMachine.VirtualMachineBuilder()
                .withNumberOfInstances(Integer.parseInt(TestDataReader.getTestData(TESTDATA_NUMBER_OF_INSTANCES)))
                .withInstancesForWhat(TestDataReader.getTestData(TESTDATA_INSTANCE_FOR_WHAT))
                .withSoftware(TestDataReader.getTestData(TESTDATA_SOFTWARE))
                .withProvisioningModel(TestDataReader.getTestData(TESTDATA_PROVISIONING_MODEL))
                .withMachineFamily(TestDataReader.getTestData(TESTDATA_MACHINE_FAMILY))
                .withSeries(TestDataReader.getTestData(TESTDATA_SERIES))
                .withMachineType(TestDataReader.getTestData(TESTDATA_MACHINE_TYPE))
                .withGpuType(TestDataReader.getTestData(TESTDATA_GPU_TYPE))
                .withNumberOfGPU(TestDataReader.getTestData(TESTDATA_NUMBER_OF_GPU))
                .withLocalSsd(TestDataReader.getTestData(TESTDATA_LOCAL_SSD))
                .withDatacenterLocation(TestDataReader.getTestData(TESTDATA_DATACENTER_LOCATION))
                .withCommittedUsage(TestDataReader.getTestData(TESTDATA_COMMITTED_USAGE))
                .withTotalPrice(TestDataReader.getTestData(TESTDATA_TOTAL_PRICE))
                .build();

        GoogleCloudService.openLink(TestDataReader.getTestData(SEARCH_TEXT),
                TestDataReader.getTestData(CALCULATOR_LINK));
        GoogleCloudService.addInstance(instance);
    }

    @Test
    public void totalCostTest() {
        BrowserUtils.switchToTab(0);
        PricingCalculatorPage pricingCalculatorPage = new PricingCalculatorPage();
        pricingCalculatorPage.switchToFrame();
        Assert.assertEquals(pricingCalculatorPage.getTotalCost(),
                totalCost, "Failed total cost");
    }

    @Test
    public void emailEstimateTest() {
        new PricingCalculatorPage()
                .clickEmailEstimateButton();

        new YopMailMainPage()
                .openPage()
                .closePopUp()
                .fillEmail(email)
                .clickRefreshButton();

        new EmailYourEstimatePage()
                .fillEmail(email)
                .clickSubmitButton();

        String totalCostFromEmail = new YopMailDetailsPage()
                .clickRefreshButton()
                .getEstimatedCost();

        Assert.assertEquals(totalCostFromEmail, totalMonthlyCost,
                "Failed total cost from email");
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        WebDriverManager.closeDriver();
    }
}
