package test.task2;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import page.pastebin.MainPage;
import page.pastebin.ResultPage;
import test.CommonBeforeTest;

public class CreateExtendedPaste extends CommonBeforeTest {
    // test data
    private String code = "git config --global user.name  \"New Sheriff in Town\"\n" +
            "            git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
            "            git push origin master --force";
    private String syntax = "Bash";
    private String expirationPeriod = "10 Minutes";
    private String title = "how to gain dominance among developers";

    private ResultPage resultPage;

    @BeforeClass
    public void before() {
        resultPage = new MainPage(driver)
                .openPastebinPage()
                .closePopUp()
                .fillTextArea(code)
                .clickSyntaxArrow()
                .selectPopularSyntax(syntax)
                .clickExpirationArrow()
                .selectExpirationPeriod(expirationPeriod)
                .fillTitle(title)
                .clickSubmitButton();
    }

    @Test
    public void createExtendedNewPasteTitleTest() {
        Assert.assertTrue(resultPage.getResultPageTitle().contains(title),
                "Page title does not match the expected title.");
    }

    @Test
    public void createExtendedNewPasteSyntaxTest() {
        Assert.assertEquals(resultPage.getSyntaxText(), syntax,
                "Selected syntax is not displayed as expected.");
    }

    @Test
    public void createExtendedNewPasteCodeTest() {
        Assert.assertEquals(resultPage.getCodeText(), code,
                "Entered code does not match the expected code.");
    }
}