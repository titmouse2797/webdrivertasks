package service;

import model.VirtualMachine;
import page.google.MainPage;
import page.google.PricingCalculatorPage;

public class GoogleCloudService {

    public static void openLink(String searchText, String linkName) {
        new MainPage()
                .openMainPage()
                .closePopUp()
                .clickSearchButton()
                .fillSearchField(searchText)
                .submitSearch()
                .clickLink(linkName);
    }

    public static PricingCalculatorPage addInstance(VirtualMachine instance) {
        return new PricingCalculatorPage()
                .clickComputeEngine()
                .fillNumberOfInstances(instance.getNumberOfInstances())
                .fillInstancesForWhat(instance.getInstancesForWhat())
                .clickSoftwareArrow()
                .selectOption(instance.getSoftware())
                .clickProvisioningModelArrow()
                .selectOption(instance.getProvisioningModel())
                .clickMachineFamilyArrow()
                .selectOption(instance.getMachineFamily())
                .clickSeriesArrow()
                .selectOption(instance.getSeries())
                .clickMachineTypeArrow()
                .selectOption(instance.getMachineType())
                .clickAddGPUsCheckbox()
                .clickGPUTypeArrow()
                .selectOption(instance.getGpuType())
                .clickNumberOfGPUArrow()
                .selectOption(instance.getNumberOfGPU())
                .clickLocalSsdArrow()
                .selectOption(instance.getLocalSsd())
                .clickDatacenterLocationArrow()
                .selectOption(instance.getDatacenterLocation())
                .clickCommittedUsageArrow()
                .selectOption(instance.getCommittedUsage())
                .clickAddToEstimateButton();
    }
}
