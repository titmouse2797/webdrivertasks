package util;

import java.util.Random;

public class StringUtils {
    private static final String ALPHANUMERIC_ALL_CAPS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static Random random = new Random();
    private static String emailDomain = "@popol.fr.nf";

    public static String getRandomString(int stringLength) {
        StringBuilder stringBuilder = new StringBuilder(stringLength);
        for (int i = 0; i < stringLength; i++) {
            stringBuilder.append(ALPHANUMERIC_ALL_CAPS.charAt(random.nextInt(ALPHANUMERIC_ALL_CAPS.length())));
        }
        return stringBuilder.toString();
    }

    public static String generateEmail(int prefixSize) {
        return getRandomString(prefixSize).concat(emailDomain);
    }
}
