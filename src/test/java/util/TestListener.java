package util;

import driver.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TestListener implements ITestListener {
    private Logger log = LogManager.getRootLogger();

    public void onTestStart(ITestResult iTestResult) {
        log.info("Test started: " + iTestResult.getMethod().getMethodName());
    }

    public void onTestSuccess(ITestResult iTestResult) {
        log.info("Test passed: " + iTestResult.getMethod().getMethodName());
    }

    public void onTestFailure(ITestResult iTestResult) {
        log.error("Test failed: " + iTestResult.getMethod().getMethodName());
        saveScreenshot();
    }

    public void onTestSkipped(ITestResult iTestResult) {
        log.warn("Test skipped: " + iTestResult.getMethod().getMethodName());
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        log.info("Test failed within success percentage: " + iTestResult.getMethod().getMethodName());
    }

    public void onStart(ITestContext iTestContext) {
        log.info("Test suite started: " + iTestContext.getSuite().getName());
    }

    public void onFinish(ITestContext iTestContext) {
        log.info("Test suite finished: " + iTestContext.getSuite().getName());
    }

    private void saveScreenshot() {
        File screenCapture = ((TakesScreenshot) WebDriverManager
                .getInstance())
                .getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenCapture, new File(
                    ".//target/screenshots/"
                            + getCurrentTimeAsString() +
                            ".png"));
        } catch (IOException e) {
            log.error("Failed to save screenshot: " + e.getLocalizedMessage());
        }
    }

    private String getCurrentTimeAsString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd_HH-mm-ss");
        return ZonedDateTime.now().format(formatter);
    }
}
