package util;

import driver.WebDriverManager;
import org.openqa.selenium.JavascriptExecutor;

import java.util.ArrayList;

public class BrowserUtils {
    public static void switchToTab(int tabIndex) {
        ArrayList<String> tabs = new ArrayList<>(WebDriverManager.getInstance().getWindowHandles());
        WebDriverManager.getInstance().switchTo().window(tabs.get(tabIndex));
    }

    public static void openNewTab() {
        ((JavascriptExecutor) WebDriverManager.getInstance()).executeScript("window.open()");
    }

    public static void switchToFrame(int index) {
        WebDriverManager.getInstance().switchTo().frame(index);
    }

    public static void switchToFrame(String frameName) {
        WebDriverManager.getInstance().switchTo().frame(frameName);
    }
}
