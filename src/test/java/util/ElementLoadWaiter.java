package util;

import driver.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ElementLoadWaiter {

    public static int DEFAULT_WAIT = 30;

    public static WebElement waitForElementToBeClickable(WebElement webElement) {
        return new WebDriverWait(WebDriverManager.getInstance(), Duration.ofSeconds(DEFAULT_WAIT))
                .until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public static WebElement waitForElementToBeClickable(By locator) {
        return new WebDriverWait(WebDriverManager.getInstance(), Duration.ofSeconds(DEFAULT_WAIT))
                .until(ExpectedConditions.elementToBeClickable(locator));
    }

    public static WebElement waitForElementToBeVisible(WebElement webElement) {
        return new WebDriverWait(WebDriverManager.getInstance(), Duration.ofSeconds(DEFAULT_WAIT))
                .until(ExpectedConditions.visibilityOf(webElement));
    }
}
