package driver;

import java.util.ResourceBundle;

public class ConfigWebDriver {
    private  static final ResourceBundle bundle = ResourceBundle.getBundle(System.getProperty("env"));
    public enum BrowserType {
        CHROME,
        FIREFOX
    }

    public enum PlatformType {
       WEB
    }

    public static PlatformType getPlatformType(){
        return PlatformType.valueOf(bundle.getString("platform").toUpperCase());
    }
    public static BrowserType getBrowserType(){
        return BrowserType.valueOf(bundle.getString("browser").toUpperCase());
    }

}
