package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverManager {
    private static final ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    public static WebDriver getInstance() {
        if (driver.get() == null) {
            driver.set(createDriver());
        }
        return driver.get();
    }

    private static WebDriver createDriver() {
        WebDriver driver = null;
        switch (ConfigWebDriver.getPlatformType()) {
            case WEB:
                switch (ConfigWebDriver.getBrowserType()) {
                    case CHROME:
                        driver = new ChromeDriver();
                        break;
                    case FIREFOX:
                        driver = new FirefoxDriver();
                        break;
                }
                driver.manage().window().maximize();
                break;
        }
        return driver;
    }

    public static void closeDriver() {
        if (driver.get() != null) {
            driver.get().quit();
            driver.remove();
        }
    }
}
