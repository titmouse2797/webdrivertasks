package page.google;

import driver.WebDriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import util.BrowserUtils;
import util.ElementLoadWaiter;

public class YopMailMainPage extends AbstractWebPage {
    public String searchPageUrl = "https://yopmail.com/";

    @FindBy(xpath = "//button[@id='accept']")
    private WebElement closePopUpButton;
    @FindBy(xpath = "//input[@class='ycptinput']")
    private WebElement emailInputField;
    @FindBy(xpath = "//div[@id='refreshbut']/button")
    private WebElement refreshButton;

    public YopMailMainPage openPage() {
        BrowserUtils.openNewTab();
        BrowserUtils.switchToTab(1);
        WebDriverManager.getInstance().get(searchPageUrl);
        return this;
    }

    public YopMailMainPage closePopUp() {
        ElementLoadWaiter.waitForElementToBeVisible(closePopUpButton);
        closePopUpButton.click();
        return this;
    }

    public YopMailMainPage fillEmail(String email) {
        emailInputField.sendKeys(email);
        return this;
    }

    public YopMailDetailsPage clickRefreshButton() {
        refreshButton.click();
        return new YopMailDetailsPage();
    }

    @Override
    public void switchToFrame() {
        BrowserUtils.switchToFrame(iframeName);
    }
}
