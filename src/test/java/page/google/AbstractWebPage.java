package page.google;

import driver.WebDriverManager;
import org.openqa.selenium.support.PageFactory;

public abstract class AbstractWebPage {
    protected String iframeName = "myFrame";

    public AbstractWebPage() {
        PageFactory.initElements(WebDriverManager.getInstance(), this);
    }

    public abstract void switchToFrame();
}


