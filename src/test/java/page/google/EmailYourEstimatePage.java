package page.google;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import util.BrowserUtils;

public class EmailYourEstimatePage extends AbstractWebPage {

    @FindBy(xpath = "//button[contains(@ng-click,'emailQuote.emailQuote')]")
    private WebElement submitButton;

    @FindBy(xpath = "//input[@type='email']")
    private WebElement emailInput;

    public EmailYourEstimatePage fillEmail(String email) {
        BrowserUtils.switchToTab(0);
        switchToFrame();
        emailInput.sendKeys(email);
        return this;
    }

    public PricingCalculatorPage clickSubmitButton() {
        submitButton.click();
        return new PricingCalculatorPage();
    }

    @Override
    public void switchToFrame() {
        BrowserUtils.switchToFrame(0);
        BrowserUtils.switchToFrame(iframeName);
    }
}
