package page.google;

import driver.WebDriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import util.BrowserUtils;
import util.ElementLoadWaiter;

public class MainPage extends AbstractWebPage {
    public String mainPageUrl = "https://cloud.google.com/";

    @FindBy(xpath = "//button[@class='pvUife']")
    private WebElement closePopUpButton;

    @FindBy(xpath = "//input[@placeholder='Search']")
    private WebElement searchInput;

    public MainPage openMainPage() {
        WebDriverManager.getInstance().get(mainPageUrl);
        ElementLoadWaiter.waitForElementToBeVisible(searchInput);
        return this;
    }

    public MainPage clickSearchButton() {
        searchInput.click();
        return this;
    }

    public MainPage closePopUp() {
        ElementLoadWaiter.waitForElementToBeVisible(closePopUpButton);
        closePopUpButton.click();
        return this;
    }

    public MainPage fillSearchField(String searchText) {
        searchInput.sendKeys(searchText);
        return this;
    }

    public SearchResultPage submitSearch() {
        searchInput.submit();
        return new SearchResultPage();
    }

    @Override
    public void switchToFrame() {
        BrowserUtils.switchToFrame(iframeName);
    }
}
