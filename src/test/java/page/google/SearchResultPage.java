package page.google;

import driver.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import util.BrowserUtils;

public class SearchResultPage extends AbstractWebPage {
    private String linkLocator = "//div[@class='gs-title']//b[(contains(text(), '%s'))]";

    public void clickLink(String linkName) {
        WebElement calculatorLink = WebDriverManager.getInstance()
                .findElement(By.xpath(String.format(linkLocator, linkName)));
        calculatorLink.click();
    }

    @Override
    public void switchToFrame() {
        BrowserUtils.switchToFrame(iframeName);
    }
}
