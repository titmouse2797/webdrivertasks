package page.google;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import util.BrowserUtils;
import util.ElementLoadWaiter;

public class PricingCalculatorPage extends AbstractWebPage {
    // main tab
    @FindBy(xpath = "(//div[@title='Compute Engine'])[1]")
    private WebElement computeEngineTab;
    // inputs
    @FindBy(xpath = "//input[contains(@ng-model,'computeServer.quantity')]")
    private WebElement numberOfInstancesInput;
    @FindBy(xpath = "//input[contains(@ng-model,'computeServer.label')]")
    private WebElement instancesForWhatInput;
    // selectors
    @FindBy(xpath = "//md-select[contains(@ng-model,'computeServer.os')]")
    private WebElement softwareArrow;
    @FindBy(xpath = "//md-select[contains(@ng-model,'computeServer.class')]")
    private WebElement provisioningModelArrow;
    @FindBy(xpath = "//md-select[contains(@ng-model,'computeServer.family')]")
    private WebElement machineFamilyArrow;
    @FindBy(xpath = "//md-select[contains(@ng-model,'computeServer.series')]")
    private WebElement seriesArrow;
    @FindBy(xpath = "//md-select[contains(@ng-model,'computeServer.instance')]")
    private WebElement machineTypeArrow;
    @FindBy(xpath = "//md-select[contains(@ng-model,'computeServer.gpuType')]")
    private WebElement gpuTypeArrow;
    @FindBy(xpath = "//md-select[contains(@ng-model,'computeServer.gpuCount')]")
    private WebElement numberOfGPUArrow;
    @FindBy(xpath = "//md-select[contains(@ng-model,'computeServer.ssd')]")
    private WebElement localSsd;
    @FindBy(xpath = "//md-select[contains(@ng-model,'computeServer.location')]")
    private WebElement datacenterLocation;
    @FindBy(xpath = "//md-select[contains(@ng-model,'computeServer.cud')]")
    private WebElement committedUsage;
    // common locator for selectors
    private String optionLocator = "//div[contains(@class,'md-active')]//md-option/div[(contains(text(), '%s'))]";
    @FindBy(xpath = "//md-checkbox[contains(@ng-model,'computeServer.addGPUs')]")
    private WebElement addGPUsCheckbox;
    @FindBy(xpath = "//button[contains(@ng-click,'addComputeServer')]")
    private WebElement addToEstimateButton;
    @FindBy(xpath = "//div[@class='cpc-cart-total']//b[@class='ng-binding']")
    private WebElement totalCostLabel;
    @FindBy(xpath = "//button[@title='Email Estimate']")
    private WebElement emailEstimateButton;
    @FindBy(xpath = "//md-backdrop[contains(@class,'md-select')]")
    private WebElement backDrop;

    public PricingCalculatorPage clickComputeEngine() {
        switchToFrame();
        computeEngineTab.click();
        return this;
    }

    public PricingCalculatorPage fillNumberOfInstances(int number) {
        numberOfInstancesInput.sendKeys(String.valueOf(number));
        return this;
    }

    public PricingCalculatorPage fillInstancesForWhat(String goal) {
        instancesForWhatInput.sendKeys(goal);
        return this;
    }

    public PricingCalculatorPage clickSoftwareArrow() {
        softwareArrow.click();
        return this;
    }

    public PricingCalculatorPage clickProvisioningModelArrow() {
        provisioningModelArrow.click();
        return this;
    }

    public PricingCalculatorPage clickMachineFamilyArrow() {
        machineFamilyArrow.click();
        return this;
    }

    public PricingCalculatorPage clickSeriesArrow() {
        seriesArrow.click();
        return this;
    }

    public PricingCalculatorPage clickMachineTypeArrow() {
        machineTypeArrow.click();
        return this;
    }

    public PricingCalculatorPage clickGPUTypeArrow() {
        gpuTypeArrow.click();
        return this;
    }

    public PricingCalculatorPage clickNumberOfGPUArrow() {
        numberOfGPUArrow.click();
        ElementLoadWaiter.waitForElementToBeClickable(backDrop);
        return this;
    }

    public PricingCalculatorPage clickLocalSsdArrow() {
        localSsd.click();
        return this;
    }

    public PricingCalculatorPage clickDatacenterLocationArrow() {
        datacenterLocation.click();
        return this;
    }

    public PricingCalculatorPage clickCommittedUsageArrow() {
        committedUsage.click();
        return this;
    }

    public PricingCalculatorPage clickAddGPUsCheckbox() {
        addGPUsCheckbox.click();
        return this;
    }

    public PricingCalculatorPage clickAddToEstimateButton() {
        addToEstimateButton.click();
        return this;
    }

    public PricingCalculatorPage selectOption(String software) {
        WebElement option = ElementLoadWaiter.waitForElementToBeClickable(
                By.xpath(String.format(optionLocator, software)));
        option.click();
        return this;
    }

    public String getTotalCost() {
        return totalCostLabel.getText();
    }

    public EmailYourEstimatePage clickEmailEstimateButton() {
        emailEstimateButton.click();
        return new EmailYourEstimatePage();
    }

    @Override
    public void switchToFrame() {
        BrowserUtils.switchToFrame(0);
        BrowserUtils.switchToFrame(iframeName);
    }
}
