package page.google;

import driver.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import util.BrowserUtils;
import util.ElementLoadWaiter;

import java.time.Duration;

public class YopMailDetailsPage extends AbstractWebPage {
    private String frameName = "ifmail";
    private String expectedMailCountOnPage = "1 mail";
    @FindBy(xpath = "//div[@class='wminboxheader']")
    private WebElement mailHeader;
    @FindBy(xpath = "//h3[contains(text(),'Total Estimated')]/ancestor::tr[1]")
    private WebElement totalEstimatedEmailText;
    @FindBy(xpath = "//button[@id='refresh']")
    private WebElement refreshButton;

    public YopMailDetailsPage clickRefreshButton() {
        BrowserUtils.switchToTab(1);
        Wait<WebDriver> fluentWait = new FluentWait<>(WebDriverManager.getInstance())
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(org.openqa.selenium.NoSuchElementException.class);
        fluentWait.until(webDriver -> {
            refreshButton.click();
            return mailHeader.getText().contains(expectedMailCountOnPage);
        });
        switchToFrame();
        return this;
    }

    public String getEstimatedCost() {
        ElementLoadWaiter.waitForElementToBeVisible(totalEstimatedEmailText);;
        return totalEstimatedEmailText.getText();
    }

    @Override
    public void switchToFrame() {
        BrowserUtils.switchToFrame(frameName);
    }
}
