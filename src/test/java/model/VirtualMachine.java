package model;

public class VirtualMachine {
    private int numberOfInstances;
    private String instancesForWhat;
    private String software;
    private String provisioningModel;
    private String machineFamily;
    private String series;
    private String machineType;
    private String gpuType;
    private String numberOfGPU;
    private String localSsd;
    private String datacenterLocation;
    private String committedUsage;
    private String totalPrice;

    public int getNumberOfInstances() {
        return numberOfInstances;
    }

    public String getInstancesForWhat() {
        return instancesForWhat;
    }

    public String getSoftware() {
        return software;
    }

    public String getProvisioningModel() {
        return provisioningModel;
    }

    public String getMachineFamily() {
        return machineFamily;
    }

    public String getSeries() {
        return series;
    }

    public String getMachineType() {
        return machineType;
    }

    public String getGpuType() {
        return gpuType;
    }

    public String getNumberOfGPU() {
        return numberOfGPU;
    }

    public String getLocalSsd() {
        return localSsd;
    }

    public String getDatacenterLocation() {
        return datacenterLocation;
    }

    public String getCommittedUsage() {
        return committedUsage;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public static class VirtualMachineBuilder {
        private int numberOfInstances;
        private String instancesForWhat;
        private String software;
        private String provisioningModel;
        private String machineFamily;
        private String series;
        private String machineType;
        private String gpuType;
        private String numberOfGPU;
        private String localSsd;
        private String datacenterLocation;
        private String committedUsage;
        private String totalPrice;

        public VirtualMachineBuilder() {
        }

        public VirtualMachineBuilder withNumberOfInstances(int numberOfInstances) {
            this.numberOfInstances = numberOfInstances;
            return this;
        }

        public VirtualMachineBuilder withInstancesForWhat(String instancesForWhat) {
            this.instancesForWhat = instancesForWhat;
            return this;
        }

        public VirtualMachineBuilder withSoftware(String software) {
            this.software = software;
            return this;
        }

        public VirtualMachineBuilder withProvisioningModel(String provisioningModel) {
            this.provisioningModel = provisioningModel;
            return this;
        }

        public VirtualMachineBuilder withMachineFamily(String machineFamily) {
            this.machineFamily = machineFamily;
            return this;
        }

        public VirtualMachineBuilder withSeries(String series) {
            this.series = series;
            return this;
        }

        public VirtualMachineBuilder withMachineType(String machineType) {
            this.machineType = machineType;
            return this;
        }

        public VirtualMachineBuilder withGpuType(String gpuType) {
            this.gpuType = gpuType;
            return this;
        }

        public VirtualMachineBuilder withNumberOfGPU(String numberOfGPU) {
            this.numberOfGPU = numberOfGPU;
            return this;
        }

        public VirtualMachineBuilder withLocalSsd(String localSsd) {
            this.localSsd = localSsd;
            return this;
        }

        public VirtualMachineBuilder withDatacenterLocation(String datacenterLocation) {
            this.datacenterLocation = datacenterLocation;
            return this;
        }

        public VirtualMachineBuilder withCommittedUsage(String committedUsage) {
            this.committedUsage = committedUsage;
            return this;
        }

        public VirtualMachineBuilder withTotalPrice(String totalPrice) {
            this.totalPrice = totalPrice;
            return this;
        }

        public VirtualMachine build() {
            VirtualMachine instance = new VirtualMachine();
            instance.numberOfInstances = this.numberOfInstances;
            instance.instancesForWhat = this.instancesForWhat;
            instance.software = this.software;
            instance.provisioningModel = this.provisioningModel;
            instance.machineFamily = this.machineFamily;
            instance.series = this.series;
            instance.machineType = this.machineType;
            instance.gpuType = this.gpuType;
            instance.numberOfGPU = this.numberOfGPU;
            instance.localSsd = this.localSsd;
            instance.datacenterLocation = this.datacenterLocation;
            instance.committedUsage = this.committedUsage;
            instance.totalPrice = this.totalPrice;
            return instance;
        }
    }
}
